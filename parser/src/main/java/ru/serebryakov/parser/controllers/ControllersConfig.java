package ru.serebryakov.parser.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;

@Configuration
public class ControllersConfig {


    /**
     * Главное окно
     */
    @Value("${main.window.fxml.path}")
    private String pathToMainWindowMarkup;

    @Bean(name = "mainView")
    public View getMainView() throws IOException {
        return loadView(pathToMainWindowMarkup);
    }

    @Bean
    public MainWindowController getMainWindowController() throws IOException {
        return (MainWindowController) getMainView().getController();
    }

    /**
     * Окно справки
     */
    @Value("${reference.window.fxml.path}")
    private String pathToReferenceWindowMarkup;

    @Bean(name = "referenceView")
    public View getReferenceView() throws IOException {
        return loadView(pathToReferenceWindowMarkup);
    }

    @Bean
    public ReferenceWindowController getReferenceWindowController() throws IOException {
        return (ReferenceWindowController) getReferenceView().getController();
    }

    /**
     * Окно экспорта
     */
    @Value("${export.window.fxml.path}")
    private String pathToExportWindowMarkup;

    @Bean(name = "exportView")
    public View getExportView() throws IOException {
        return loadView(pathToExportWindowMarkup);
    }

    @Bean
    public ExportWindowController getExportWindowController() throws IOException {
        return (ExportWindowController) getExportView().getController();
    }

    /**
     * Окно бэкапа
     */
    @Value("${backup.window.fxml.path}")
    private String pathToBackupWindowMarkup;

    @Bean(name = "backupView")
    public View getBackupView() throws IOException {
        return loadView(pathToBackupWindowMarkup);
    }

    @Bean
    public BackupWindowController getBackupWindowController() throws IOException {
        return (BackupWindowController) getBackupView().getController();
    }


    protected View loadView(String path) throws IOException {
        InputStream fxmlStream;
        fxmlStream = getClass().getClassLoader().getResourceAsStream(path);
        FXMLLoader loader = new FXMLLoader();
        loader.load(fxmlStream);
        return new View(loader.getRoot(), loader.getController());
    }


    @AllArgsConstructor
    public class View {

        @Getter
        @Setter
        private Parent view;

        @Getter
        @Setter
        private Object controller;

    }

}
