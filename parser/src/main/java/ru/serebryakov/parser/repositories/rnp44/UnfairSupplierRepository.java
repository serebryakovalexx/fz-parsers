package ru.serebryakov.parser.repositories.rnp44;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.serebryakov.parser.models.rnp44.UnfairSupplier;

public interface UnfairSupplierRepository extends JpaRepository<UnfairSupplier, Long> {
}
