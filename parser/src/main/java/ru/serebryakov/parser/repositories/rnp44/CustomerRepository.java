package ru.serebryakov.parser.repositories.rnp44;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.serebryakov.parser.models.rnp44.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long>{
}
