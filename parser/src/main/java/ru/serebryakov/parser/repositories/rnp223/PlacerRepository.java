package ru.serebryakov.parser.repositories.rnp223;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.serebryakov.parser.models.rnp223.Placer;

public interface PlacerRepository extends JpaRepository<Placer, Long>{
}
