package ru.serebryakov.parser.repositories.rnp223;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.serebryakov.parser.models.rnp223.Supplier;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {
}
