package ru.serebryakov.parser.ftp;

import java.util.List;

public interface FtpClientService {

    boolean searchAndDownloadFile(String workingDirectory, List<String> timePeriods, String downloadDirectory);

}
