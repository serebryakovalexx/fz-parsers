package ru.serebryakov.parser.ftp.rnp;


import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.serebryakov.parser.ftp.FtpClientService;

import java.io.*;
import java.util.List;

@Slf4j
@Component(value = "ftp.rnp.44")
public class FtpClientForRNP44 implements FtpClientService {

    @Value("${ftp.rnp.44.workingDirectory}")
    private String initialWorkingDirectoryForFtpRnp44;

    @Value("${directory.name.rnp.44}")
    private String directoryNameForRnp44;

    @Getter
    @Value("${ftp.rnp.44.host}")
    private String host;

    @Value("${ftp.rnp.44.user}")
    private String username;

    @Value("${ftp.rnp.44.password}")
    private String password;


    // TODO: вернуть булево значение
    public boolean searchAndDownloadFile(String workingDirectory, List<String> timePeriods, String downloadDirectory){

        FTPClient ftpClient = new FTPClient();

        if(workingDirectory == null){
            workingDirectory = initialWorkingDirectoryForFtpRnp44;
        }

        try {
            ftpClient.connect(host);
            if (!ftpClient.login(username, password)) {
                ftpClient.logout();
                log.error(getClass().getName() + ": Не удалось подключиться(неверный логин или пароль)");
                return false;
            }

            if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
                ftpClient.disconnect();
                log.error(getClass().getName() + ": Не удалось подключиться(отрицательный ответ сервера)");
                return false;
            }

            ftpClient.enterLocalPassiveMode();

            ftpClient.changeWorkingDirectory(workingDirectory);

            FTPFile[] ftpFiles = ftpClient.listFiles();

            if (ftpFiles != null && ftpFiles.length > 0) {
                for (FTPFile ftpFile : ftpFiles) {
                    if (ftpFile.isDirectory()) {
                        searchAndDownloadFile(workingDirectory + ftpFile.getName() + "/", timePeriods, downloadDirectory);
                    } else if (ftpFile.isFile() && ftpFile.getName().endsWith(".xml.zip")) {
                        if (timePeriods.contains("за всё время")) {
                            if (ftpFile.getName().contains("unfairSupplier_")) {

                                int beginIndex = ftpFile.getName().indexOf("unfairSupplier_") + "unfairSupplier_".length();
                                String year = ftpFile.getName().substring(beginIndex, beginIndex + 4);
                                downloadFile(ftpClient, ftpFile.getName().trim(), year.trim(), downloadDirectory.trim());

                            }
                        } else {
                            timePeriods.forEach(timePeriod -> {
                                if (ftpFile.getName().contains("unfairSupplier_" + timePeriod)) {
                                    try {
                                        downloadFile(ftpClient, ftpFile.getName().trim(), timePeriod.trim(), downloadDirectory.trim());
                                    } catch (IOException e) {
                                        log.warn(getClass().getName() + ": При скачивании файла возникла ошибка");
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } catch (IOException ex) {
            log.error(getClass().getName() + ": Ошибка");
            return false;
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                log.warn(getClass().getName() + ": Ошибка при закрытии подключения");
            }
        }

        return true;
    }

    private void downloadFile(FTPClient ftpClient, String fileName, String year, String uploadDirectory) throws IOException {

        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);

        File localDirectory = new File(uploadDirectory + File.separatorChar + directoryNameForRnp44 + File.separatorChar + year);
        File localFile = new File(uploadDirectory + File.separatorChar + directoryNameForRnp44 + File.separatorChar + year + File.separatorChar + fileName);

        localDirectory.mkdirs();

        if(localFile.exists()){
            localFile.delete();
            localFile.createNewFile();
        }

        try (OutputStream os = new BufferedOutputStream(new FileOutputStream(localFile))) {
            boolean uploadStatus = ftpClient.retrieveFile(fileName, os);
            if (!uploadStatus) {
                log.warn(getClass().getName() + ": Не удалось скачать файл");
            }
        }

        ftpClient.setFileType(FTP.ASCII_FILE_TYPE);
    }

}
