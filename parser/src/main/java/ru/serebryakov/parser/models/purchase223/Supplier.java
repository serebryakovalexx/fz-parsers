package ru.serebryakov.parser.models.purchase223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

/**
 * архив с xml файлами находится в папке (в каждом типе purchaseProtocol список lotApplicationsList обрабатывается по-разному):
 * /out/published/{имя_региона}/purchaseProtocol/daily/
 * /out/published/{имя_региона}/purchaseProtocolZK/daily/
 * /out/published/{имя_региона}/purchaseProtocolVK/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAEP/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAAE/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAOA/daily/
 * /out/published/{имя_региона}/purchaseProtocolOSZ/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZOK/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZOA/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZ1AE/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZ2AE/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZAE/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAAE95/daily/
 * сущность Supplier находится по пути: ns2:purchaseProtocol{тип протокола} -> ns2:body -> ns2:item ->
 * ns2:lotApplicationsList -> ns2:protocolLotApplications -> ns2:lot -> ns2:application
 */
@Data
@Entity
@Table(name = "supplier", schema = "purchase223")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "supplier_id")
    private Long id;


    /**
     * в purchaseProtocol: ... -> ns2:applicationNumber
     * в purchaseProtocolZK: ... -> ns2:applicationNumber
     * в purchaseProtocolVK: ... -> ns2:applicationNumber
     * в purchaseProtocolPAEP: ... -> ns2:applicationNumber
     * в purchaseProtocolPAAE: ... -> ns2:applicationNumber
     */
    @Column(name = "application_number", columnDefinition = "text")
    private String applicationNumber;

    /**
     * в purchaseProtocol: ... -> ns2:supplierInfo -> name
     * в purchaseProtocolZK: ... -> ns2:participantInfo -> name
     *                              ns2:supplierInfo -> name (если ns2:participantInfo не найден)
     * в purchaseProtocolVK: ... -> ns2:supplierInfo -> name
     * в purchaseProtocolPAEP: ... -> ns2:supplierInfo -> name
     * в purchaseProtocolPAAE: ... -> ns2:supplierInfo -> name
     */
    @Column(name = "name", columnDefinition = "text")
    private String name;

    /**
     * в purchaseProtocol: ... -> ns2:supplierInfo -> inn
     * в purchaseProtocolZK: ... -> ns2:participantInfo -> inn
     *                              ns2:supplierInfo -> inn (если ns2:participantInfo не найден)
     * в purchaseProtocolVK: ... -> ns2:supplierInfo -> inn
     * в purchaseProtocolPAEP: ... -> ns2:supplierInfo -> inn
     * в purchaseProtocolPAAE: ... -> ns2:supplierInfo -> inn
     */
    @Column(name = "inn", columnDefinition = "text")
    private String inn;

    /**
     * в purchaseProtocol: ... -> ns2:supplierInfo -> kpp
     * в purchaseProtocolZK: ... -> ns2:participantInfo -> kpp
     *                              ns2:supplierInfo -> kpp (если ns2:participantInfo не найден)
     * в purchaseProtocolVK: ... -> ns2:supplierInfo -> kpp
     * в purchaseProtocolPAEP: ... -> ns2:supplierInfo -> kpp
     * в purchaseProtocolPAAE: ... -> ns2:supplierInfo -> kpp
     */
    @Column(name = "kpp", columnDefinition = "text")
    private String kpp;

    /**
     * в purchaseProtocol: ... -> ns2:supplierInfo -> ogrn
     * в purchaseProtocolZK: ... -> ns2:participantInfo -> ogrn
     *                              ns2:supplierInfo -> ogrn (если ns2:participantInfo не найден)
     * в purchaseProtocolVK: ... -> ns2:supplierInfo -> ogrn
     * в purchaseProtocolPAEP: ... -> ns2:supplierInfo -> ogrn
     * в purchaseProtocolPAAE: ... -> ns2:supplierInfo -> ogrn
     */
    @Column(name = "ogrn", columnDefinition = "text")
    private String ogrn;


    /**
     * в purchaseProtocol: false, если отсутсвует элемент ... -> ns2:supplierInfo -> nonResidentInfo
     * в purchaseProtocolZK: false, если отсутсвует элемент ... -> ns2:supplierInfo -> nonResidentInfo (или ещё может быть у ns2:participantInfo???)
     * в purchaseProtocolVK: false, если отсутсвует элемент ... -> ns2:supplierInfo -> nonResidentInfo
     * в purchaseProtocolPAEP: false, если отсутсвует элемент ... -> ns2:supplierInfo -> nonResidentInfo
     * в purchaseProtocolPAAE: false, если отсутсвует элемент ... -> ns2:supplierInfo -> nonResidentInfo
     */
    @Column(name = "non_resident")
    private Boolean nonResident;

    /**
     * в purchaseProtocol: ... -> ns2:supplierInfo -> ns2:nonResidentInfo -> code
     * в purchaseProtocolZK: ... -> ns2:supplierInfo -> ns2:nonResidentInfo -> code (или ещё ... -> ns2:participantInfo -> ns2:nonResidentInfo -> code ??)
     * в purchaseProtocolVK: ... -> ns2:supplierInfo -> ns2:nonResidentInfo -> code
     * в purchaseProtocolPAEP: ... -> ns2:supplierInfo -> ns2:nonResidentInfo -> code
     * в purchaseProtocolPAAE: ... -> ns2:supplierInfo -> ns2:nonResidentInfo -> code
     */
    @Column(name = "code", columnDefinition = "text")
    private String code;

    /**
     * в purchaseProtocol: ... -> ns2:supplierInfo -> nonResidentInfo -> info
     * в purchaseProtocolZK: ... -> ns2:supplierInfo -> nonResidentInfo -> info (или ещё ... -> ns2:participantInfo -> nonResidentInfo -> info???)
     * в purchaseProtocolVK: ... -> ns2:supplierInfo -> ns2:nonResidentInfo -> info
     * в purchaseProtocolPAEP: ... -> ns2:supplierInfo -> ns2:nonResidentInfo -> info
     * в purchaseProtocolPAAE: ... -> ns2:supplierInfo -> ns2:nonResidentInfo -> info
     */
    @Column(name = "nr_info", columnDefinition = "text")
    private String nrInfo;

    /**
     * в purchaseProtocol: ... -> ns2:price
     * в purchaseProtocolZK: ... -> ns2:price
     * в purchaseProtocolVK: ... -> ns2:price???
     * в purchaseProtocolPAEP: ... -> ns2:lastPrice
     * в purchaseProtocolPAAE: ... -> ns2:lastPrice
     */
    @Column(name = "price", columnDefinition = "text")
    private String price;

    /**
     * в purchaseProtocol: ... -> ns2:currency -> code
     * в purchaseProtocolZK: ... -> ns2:currency -> code
     * в purchaseProtocolVK: ... -> ns2:currency -> code
     * в purchaseProtocolPAEP: ... -> ns2:currency -> code
     * в purchaseProtocolPAAE: ... -> ns2:currency -> code
     */
    @Column(name = "currency_code", columnDefinition = "text")
    private String currencyCode;

    /**
     * в purchaseProtocol: ... -> ns2:accepted
     * в purchaseProtocolZK: ... -> ns2:accepted
     */
    @Column(name = "accepted", columnDefinition = "text")
    private String accepted;

    /**
     * в purchaseProtocolZK: ... -> ns2:rejectionReason
     */
    @Column(name = "rejection_reason", columnDefinition = "text")
    private String rejectionReason;

    /**
     * в purchaseProtocolPAEP: всегда true
     */
    @Column(name = "winner", columnDefinition = "text")
    private Boolean winner;

    /**
     * в purchaseProtocol: ... -> ns2:winnerIndication == F, то place = 1 иначе 0
     * в purchaseProtocolZK: ... -> ns2:winnerIndication == W, то place = 1 иначе 0
     * в purchaseProtocolPAEP: ... -> всегда 1
     * в purchaseProtocolPAAE: ... -> ns2:applicationPlace == F, то place = 1 иначе 0
     */
    @Column(name = "place", columnDefinition = "text")
    private Integer place;

    @OneToMany(mappedBy = "supplier", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Participation> participations;

    @OneToMany(mappedBy = "supplier", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Contract> contracts;

}
