package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Base {

    private String baseName;
    private String baseDate;
    private String baseNumber;

}
