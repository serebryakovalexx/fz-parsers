package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "founder", schema = "rnp44")
public class Founder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "founder_id")
    private Long id;

    @Column(name = "inn", columnDefinition = "text")
    private String inn;

    @Column(name = "name", columnDefinition = "text")
    private String name;

    @ManyToMany(mappedBy = "founders", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<UnfairSupplierInformation> unfairSupplierInformations;

}
