package ru.serebryakov.parser.models.rnp223;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "prev_document", schema = "rnp223")
public class PrevDocument {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "prev_document_id")
    private Long id;

    @Column(name = "file_name", columnDefinition = "text")
    private String fileName;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "url", columnDefinition = "text")
    private String url;

    @Column(name = "content", columnDefinition = "text")
    private String content;

    @Column(name = "registration_number", columnDefinition = "text")
    private String registrationNumber;

    @Column(name = "create_date_time", columnDefinition = "text")
    private String createDateTime;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "attachments_id")
    private Attachments attachments;


}
