package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "customer", schema = "rnp44")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private Long id;

    @Column(name = "reg_num", columnDefinition = "text")
    private String regNum;

    @Column(name = "cons_registry_num", columnDefinition = "text")
    private String consRegistryNum;

    @Column(name = "full_name", columnDefinition = "text")
    private String fullName;

    @Column(name = "short_name", columnDefinition = "text")
    private String shortName;

    @Column(name = "post_address", columnDefinition = "text")
    private String postAddress;

    @Column(name = "fact_address", columnDefinition = "text")
    private String factAddress;

    @Column(name = "inn", columnDefinition = "text")
    private String inn;

    @Column(name = "kpp", columnDefinition = "text")
    private String kpp;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<UnfairSupplier> unfairSuppliers;

}
