package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "unfair_supplier", schema = "rnp44")
public class UnfairSupplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "unfair_supplier_id")
    private Long id;

    @Column(name = "registry_num", columnDefinition = "text")
    private String registryNum;

    @Column(name = "publish_date", columnDefinition = "text")
    private String publishDate;

    @Column(name = "approve_date", columnDefinition = "text")
    private String approveDate;

    @Column(name = "state", columnDefinition = "text")
    private String state;

    @Column(name = "create_reason", columnDefinition = "text")
    private String createReason;

    @Column(name = "approve_reason", columnDefinition = "text")
    private String approveReason;

    @OneToMany(mappedBy = "unfairSupplier", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Contract> contracts;

    @OneToMany(mappedBy = "unfairSupplier", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Exclude> excludes;

    @OneToMany(mappedBy = "unfairSupplier", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Purchase> purchases;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "publish_org_id")
    private PublishOrg publishOrg;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "unfair_supplier_information_id")
    private UnfairSupplierInformation unfairSupplierInformation;


}
