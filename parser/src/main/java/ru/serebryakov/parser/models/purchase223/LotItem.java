package ru.serebryakov.parser.models.purchase223;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "lot_item", schema = "purchase223")
public class LotItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lot_item_id")
    private Long id;

    @Column(name = "number", columnDefinition = "text")
    private String number;

    @Column(name = "guid", columnDefinition = "text")
    private String guid;

    @Column(name = "okdp", columnDefinition = "text")
    private String okdp;

    @Column(name = "okdp2", columnDefinition = "text")
    private String okdp2;

    @Column(name = "okved", columnDefinition = "text")
    private String okved;

    @Column(name = "okved2", columnDefinition = "text")
    private String okved2;

    @Column(name = "okei", columnDefinition = "text")
    private String okei;

    @Column(name = "quantaty", columnDefinition = "text")
    private String quantaty;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "lot_id")
    private Lot lot;
}
