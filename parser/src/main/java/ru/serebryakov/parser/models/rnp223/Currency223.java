package ru.serebryakov.parser.models.rnp223;


import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "currency223", schema = "rnp223")
public class Currency223 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "currency223_id")
    private Long id;

    @Column(name = "code", columnDefinition = "text")
    private String code;

    @Column(name = "digital_code", columnDefinition = "text")
    private String digitalCode;

    @Column(name = "name", columnDefinition = "text")
    private String name;

    @OneToMany(mappedBy = "currency223", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<ContractInfo> contractInfos;

}
