package ru.serebryakov.parser.models.rnp44;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "currency", schema = "rnp44")
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "currency_id")
    private Long id;

    @Column(name = "code", columnDefinition = "text")
    private String code;

//   TODO: ??????
    @Column(name = "sssssssssss", columnDefinition = "text")
    private String ssss;

    @Column(name = "name", columnDefinition = "text")
    private String name;

    @OneToMany(mappedBy = "currency",  fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Contract> contracts;

}
