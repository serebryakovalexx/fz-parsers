package ru.serebryakov.parser.models.rnp223;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "common_application_info", schema = "rnp223")
public class CommonApplicationInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "common_application_info_id")
    private Long id;

    @Column(name = "include_reason", columnDefinition = "text")
    private String includeReason;

    @Column(name = "include_reason_text", columnDefinition = "text")
    private String includeReasonText;

    @Column(name = "top_secret", columnDefinition = "text")
    private String topSecret;

    @Column(name = "include_date", columnDefinition = "text")
    private String includeDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "control_agency_id")
    private ControlAgency controlAgency;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "purchase_info_id")
    private PurchaseInfo purchaseInfo;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "dishonest_supplier_data_id")
    private DishonestSupplierData dishonestSupplierData;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "contract_info_id")
    private ContractInfo contractInfo;

}
