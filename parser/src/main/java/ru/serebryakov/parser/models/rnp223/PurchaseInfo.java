package ru.serebryakov.parser.models.rnp223;


import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "purchase_info", schema = "rnp223")
public class PurchaseInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "purchase_info_id")
    private Long id;

    @Column(name = "purchase_notice_number", columnDefinition = "text")
    private String purchaseNoticeNumber;

    @Column(name = "name", columnDefinition = "text")
    private String name;

    @Column(name = "document_number", columnDefinition = "text")
    private String documentNumber;

    @Column(name = "document_name", columnDefinition = "text")
    private String documentName;

    @Column(name = "document_date", columnDefinition = "text")
    private String documentDate;

    @Column(name = "purchase_method_code", columnDefinition = "text")
    private String purchaseMethodCode;

    @Column(name = "purchase_code_name", columnDefinition = "text")
    private String purchaseCodeName;

    @Column(name = "emergency", columnDefinition = "text")
    private String emergency;

    @Column(name = "lot_info", columnDefinition = "text")
    private String lotInfo;

    @Column(name = "purchase_date", columnDefinition = "text")
    private String purchaseDate;

    @Column(name = "summingup_date", columnDefinition = "text")
    private String summingupDate;

    @Column(name = "cancellation_date", columnDefinition = "text")
    private String cancellationDate;

    @Column(name = "publication_date_time", columnDefinition = "text")
    private String publicationDateTime;

    @OneToMany(mappedBy = "purchaseInfo", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<CommonApplicationInfo> commonApplicationInfos;

}
