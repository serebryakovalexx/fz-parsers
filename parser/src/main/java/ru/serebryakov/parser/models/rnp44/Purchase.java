package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "purchase", schema = "rnp44")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "purchase_id")
    private Long id;

    @Column(name = "purchase_number", columnDefinition = "text")
    private String purchaseNumber;

    @Column(name = "purchase_object_info", columnDefinition = "text")
    private String purchaseObjectInfo;

    @Column(name = "placing_way_name", columnDefinition = "text")
    private String placingWayName;

    @Column(name = "protocol_date", columnDefinition = "text")
    private String protocolDate;

    @Column(name = "lot_number", columnDefinition = "text")
    private String lotNumber;


    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "documentName", column = @Column(name = "document_name")),
            @AttributeOverride(name = "documentNumber", column = @Column(name = "document_number")),
            @AttributeOverride(name = "documentDate", column = @Column(name = "document_date"))
    })
    private Document document;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "unfair_supplier_id")
    private UnfairSupplier unfairSupplier;


}
