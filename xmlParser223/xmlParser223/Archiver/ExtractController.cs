﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace xmlParser223.Archiever
{
    public class ExtractController
    {
        public static void ReAcrchive(string region)
        {
            List<string> directs = Directory.GetDirectories(region).ToList();




            List<string> zipFiles = new List<string>();


            List<Task> taskList = new List<Task>();
            foreach (var drct in directs)
            {
                zipFiles = Directory.GetFiles(Path.Combine(drct, "daily")).ToList();
                
                foreach (var file in zipFiles)
                {

                    try
                    {
                        ExtractZipFile(file, drct.Replace("zip", "xml"));
                    }
                    catch (Exception err)
                    {
                        File.AppendAllText("badXML.txt", file + "\t" + err.Message + Environment.NewLine);
                    }


                }
            }

        }

        static object syncLock = new object();

        public static void ExtractZipFile(string archiveFilenameIn, string outFolder)
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                List<Task> taskList = new List<Task>();
                foreach (ZipEntry zipEntry in zf)
                {
                    taskList.Add(Task.Factory.StartNew(() => 
                    {
                        //if (!zipEntry.IsFile)
                        //{
                        //    continue;           // Ignore directories
                        //}
                        String entryFileName = zipEntry.Name;
                        // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                        // Optionally match entrynames against a selection list here to skip as desired.
                        // The unpacked length is available in the zipEntry.Size property.

                        byte[] buffer = new byte[4096];     // 4K is optimum
                        Stream zipStream = zf.GetInputStream(zipEntry);

                        // Manipulate the output filename here as desired.
                        String fullZipToPath = Path.Combine(outFolder, entryFileName);
                        string directoryName = Path.GetDirectoryName(fullZipToPath);
                        if (directoryName.Length > 0)
                            Directory.CreateDirectory(directoryName);

                        // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                        // of the file, but does not waste memory.
                        // The "using" will close the stream even if an exception occurs.

                        int i = 0;
                        lock (syncLock)
                        {
                            while (File.Exists(fullZipToPath.Replace(".xml", $"_{ i}.xml")))
                                i++;
                            using (FileStream streamWriter = File.Create(fullZipToPath.Replace(".xml", $"_{ i}.xml")))
                            {
                                StreamUtils.Copy(zipStream, streamWriter, buffer);
                            }
                        }
                    }));
                    Task.WaitAll(taskList.ToArray());
                   
                }
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
            }
        }
    }
}
