﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlParser223
{
    public class ConvertUtils
    {

        static System.Globalization.NumberStyles styleFloat = System.Globalization.NumberStyles.Float;
        //static System.Globalization.NumberStyles styleInt = System.Globalization.NumberStyles.Integer;

        /// <summary>
        /// Английская локаль. Используется в качестве дополнительного параметра IFormatProvider в функциях конвертации
        /// </summary>
        public static System.Globalization.CultureInfo enCulture = new System.Globalization.CultureInfo("en-US");



        /// <summary>
        /// Конвертирует строку в число с типом decimal с использованием английской локали (разделитель целой и дробной части - точка)
        /// </summary>
        /// <param name="value">Исходная строка</param>
        /// <param name="res">Полученное число</param>
        /// <returns>Результат выполнения операции (true - успешно, false - неудачно)</returns>
        public static decimal SafeEnStringToDecimal(string value)
        {
            if (decimal.TryParse(value, styleFloat, enCulture, out decimal res))
                return res;
            else
                res = decimal.Zero;
            return res;
        }


    }
}
