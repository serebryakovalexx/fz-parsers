﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace xmlParser223.Export
{
    public partial class ExportForm: Form
    {
        public ExportForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            (sender as Button).Enabled = false;
            string sql = sqlBox.Text;
            string fileName = saveFile.Text;
            Stopwatch st = Stopwatch.StartNew();

            ExportData.ExportQuery(sql, fileName);
            MessageBox.Show($"Данные сохранены за {st.ElapsedMilliseconds / 1000.00} секунд");
            (sender as Button).Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            (sender as Button).Enabled = false;
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "Файлы Excel (*.xlsx)|*.xlsx";
            openFileDialog.FileName = $"";
            openFileDialog.InitialDirectory = Environment.CurrentDirectory;
            if (openFileDialog.ShowDialog() == true)
            {
                MessageBox.Show("Внимание выбранный файл перезапишется!");
                saveFile.Text = openFileDialog.FileName;
            }
            (sender as Button).Enabled = true;
        }
    }
}
