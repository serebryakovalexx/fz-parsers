﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using xmlParser223.DocumentsInfo;
using static xmlParser223.ConnectionController;

namespace xmlParser223.ParserXML
{
    public class ProtocolLoader
    {
        static int SnID { get; set; }
        static int RegionID { get; set; }
        static XName GetGlobalName(string prop) => XName.Get(prop, "http://zakupki.gov.ru/223fz/types/1");
        static XName Getns2Name(string prop) => XName.Get(prop, "http://zakupki.gov.ru/223fz/purchase/1");

        public static void ParseProtocols(string path, NpgsqlConnection conn, int sID, int regionId)
        {
            SnID = sID;
            RegionID = regionId;

            List<string> directs = Directory.GetDirectories(path).Where(s => s.Contains("Protocol") && !s.Contains("Cancell")).ToList();

            foreach (var directory in directs)
            {
                List<Task> taskList = new List<Task>();
                List<string> xmlFiles = Directory.GetFiles(directory).Reverse().ToList();
                foreach (var file in xmlFiles)
                    taskList.Add(Task.Factory.StartNew(() => ParseProtocol(file)));
                Task.WaitAll(taskList.ToArray());
            }


        }

        public static void ParseZipProtocols(string regionPath, int sID, int regionId)
        {
            SnID = sID;
            RegionID = regionId;

            List<string> directs = Directory.GetDirectories(regionPath).Where(s => s.Contains("Protocol") && !s.Contains("Cancell")).ToList();

            foreach (var directory in directs)
            {
                List<string> zipFiles = Directory.GetFiles(directory + @"\daily").ToList();
                List<Task> taskList0 = new List<Task>();
                foreach (var zipFile in zipFiles)
                    taskList0.Add(Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            using (ZipArchive archive = ZipFile.OpenRead(zipFile))
                            {
                                Regex rg = new Regex(@"daily_\d+");
                                List<Task> taskList = new List<Task>();
                                foreach (ZipArchiveEntry file in archive.Entries)
                                    using (StreamReader reader = new StreamReader(file.Open()))
                                    {
                                        string data = reader.ReadToEnd();
                                        taskList.Add(Task.Factory.StartNew(() =>
                                        {
                                            string fileName = zipFile + rg.Match(file.Name).Value.Replace("daily", "");
                                            ParseProtocol(data, fileName);
                                        }));
                                    }
                                Task.WaitAll(taskList.ToArray());
                            }
                        }
                        catch (Exception err)
                        {
                            File.AppendAllText("invalidFiles.txt", zipFile + "\t" + err.Message + Environment.NewLine);
                        }
                    }));
                Task.WaitAll(taskList0.ToArray());
            }
        }


        public static void ParseProtocol(string filePath)
        {
            //Regex rg = new Regex(@"[^\\]+$");
            //string fileName = rg.Match(filePath).Value;
            string fileName = filePath;
            string sql = $"select id from file_status where id = '{fileName}'";





            string file = File.ReadAllText(filePath);
            if (file != "")
            {

                int connNum = ConnectionController.GetFreeConnectionNum();
                NpgsqlConnection conn = ConnectionController.ConnList.Where(s => s.Num == connNum).FirstOrDefault().Conn;

                conn.Open();
                try
                {
                    XDocument doc = XDocument.Parse(file);
                ProtocolInfo prt = DetermineParseProtocol(doc);

                if (prt != null)
                {





                    WriteData(prt, conn);

                    sql = $"insert into file_status (id, id_session, load_status, file_code, guid_document) values ('{fileName}', {SnID}, 1, 2, '{prt.GUID}') " +
                    $" on conflict (id) do update set load_status = 1, error = null;";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();

                }
                else
                {
                    sql = $"insert into file_status (id, id_session, load_status, file_code, error) values ('{fileName}', {SnID}, 0, 2, 'Неизвестный тип протокла')";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

            }
                catch (Exception ex)
            {

                sql = $"insert into file_status (id, id_session, load_status, error ,file_code) values ('{fileName}','{SnID}', 0, '{ex.Message}', 2) on conflict(id) do nothing";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            conn.Close();
                ConnectionController.ConnList.Where(s => s.Num == connNum).FirstOrDefault().IsFree = true;



            }



        }

        public static void ParseProtocol(string data, string fileName)
        {
            //Regex rg = new Regex(@"[^\\]+$");
            //string fileName = rg.Match(filePath).Value;
            string sql;
            NpgsqlCommand cmd;
            if (data != "")
            {
                OpenFreeConn(out var a, out var cn);
                try
                {
                    XDocument doc = XDocument.Parse(data);
                    ProtocolInfo prt = DetermineParseProtocol(doc);

                    if (prt != null)
                    {
                        WriteData(prt, cn);

                        sql = $"insert into gz.file_status (id, id_session, load_status, file_code, guid_document) values ('{fileName}', {SnID}, 1, 2, '{prt.GUID}') ";
                        cmd = new NpgsqlCommand(sql, cn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    else
                    {
                        sql = $"insert into gz.file_status (id, id_session, load_status, file_code, error) values ('{fileName}', {SnID}, 0, 2, 'Неизвестный тип протокла')";
                        cmd = new NpgsqlCommand(sql, cn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    sql = $"insert into gz.file_status (id, id_session, load_status, error ,file_code) values ('{fileName}','{SnID}', 0, '{ex.Message}', 2) on conflict(id) do nothing";
                    cmd = new NpgsqlCommand(sql, cn);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                CloseBusyConn(a, cn);
            }
        }

        static ProtocolInfo DetermineParseProtocol(XDocument doc)
        {
            string docType = doc.Root.Name.LocalName;
            XElement data = doc.Root
                             .Element(Getns2Name("body"))
                             .Element(Getns2Name("item"))
                             .Element(Getns2Name($"{docType}Data"));

            ProtocolInfo prt = new ProtocolInfo();
            List<LotInfo> lots = new List<LotInfo>();

            prt.GUID = data.Element(Getns2Name("guid")).Value;
            prt.Date = DateTime.Parse(data.Element(Getns2Name("createDateTime"))?.Value ?? "01.01.1900");
            prt.PurchaseRegNum = data.Element(Getns2Name("purchaseInfo"))?.Element(GetGlobalName("purchaseNoticeNumber"))?.Value;
            prt.PurchaseGUID = data.Element(Getns2Name("purchaseInfo"))?.Element(GetGlobalName("guid"))?.Value;
            prt.RegNum = data.Element(Getns2Name("registrationNumber"))?.Value;
            prt.Version = Convert.ToInt32(data.Element(Getns2Name("version"))?.Value ?? "0");
            prt.Type = docType;
            prt.MissedContest = data.Element(Getns2Name("missedContest"))?.Value;
            prt.MissedReason = data.Element(Getns2Name("missedReason"))?.Value;



            XElement lotData = data.Element(Getns2Name("lotApplicationsList"));
            switch (docType)
            {
                case "purchaseProtocol":
                    lots = ParsePurchaseProtocol(lotData);
                    break;
                case "purchaseProtocolZK":
                    lots = ParsePurchaseProtocolZK(lotData);
                    break;
                case "purchaseProtocolVK":
                    lots = ParsePurchaseProtocolVK(lotData);
                    break;
                case "purchaseProtocolPAEP":
                    lots = ParsePurchaseProtocolPAEP(lotData);
                    break;
                case "purchaseProtocolPAAE":
                    lots = ParsePurchaseProtocolPAAE(lotData);
                    break;
                case "purchaseProtocolPAOA":
                    lots = ParsePurchaseProtocolPAAE(lotData);
                    break;
                case "purchaseProtocolOSZ":
                    lots = ParsePurchaseProtocolOSZ(lotData);
                    break;
                case "purchaseProtocolRZOK":
                    lots = ParsePurchaseProtocolRZOK(lotData);
                    break;
                case "purchaseProtocolRZOA":
                    lots = ParsePurchaseProtocolRZOK(lotData);
                    break;
                case "purchaseProtocolRZ1AE":
                    lots = ParsePurchaseProtocolRZ1AE(lotData);
                    break;
                case "purchaseProtocolRZ2AE":
                    lots = ParsePurchaseProtocolRZ2AE(lotData);
                    break;
                case "purchaseProtocolRZAE":
                    lots = ParsePurchaseProtocolRZOK(lotData);
                    break;
                case "purchaseProtocolPAAE95":
                    lots = ParsePurchaseProtocolPAAE94(lotData);
                    break;
                default:
                    return null;
            }

            prt.Lots = lots;

            return prt;
        }

        static List<LotInfo> ParsePurchaseProtocol(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();

            var lotsElemets = data.Elements(Getns2Name("protocolLotApplications"));
            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("lot"))?.Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("lot"))?.Element(Getns2Name("ordinalNumber"))?.Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();
                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;
                    var supInfo = apl.Element(Getns2Name("supplierInfo"));
                    if (supInfo != null)
                    {
                        sp.Name = supInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = supInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = supInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = supInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else
                    {
                        supInfo = apl.Element(Getns2Name("nonResidentInfo"));
                        if (supInfo != null)
                        {
                            sp.Code = supInfo.Element(GetGlobalName("code"))?.Value;
                            sp.NRInfo = supInfo.Element(GetGlobalName("info"))?.Value;
                            sp.NonResident = "true";
                        }
                    }
                    sp.Offer = apl.Element(Getns2Name("price"))?.Value ?? "null";
                    sp.Currency = apl.Element(Getns2Name("currency"))?.Element(GetGlobalName("code")).Value;


                    sp.Accepted = apl.Element(Getns2Name("accepted"))?.Value;
                    string wn = apl.Element(Getns2Name("winnerIndication"))?.Value;
                    if (wn == "F")
                        sp.Place = 1;
                    else
                        sp.Place = 0;

                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static List<LotInfo> ParsePurchaseProtocolZK(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();
            var lotsElemets = data.Elements(Getns2Name("protocolLotApplications"));
            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("ordinalNumber")).Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();
                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;
                    var partInfo = apl.Element(Getns2Name("participantInfo"));
                    var supInfo = apl.Element(Getns2Name("supplierInfo"));
                    if (partInfo != null)
                    {
                        sp.Name = partInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = partInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = partInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = partInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else if (supInfo != null)
                    {
                        sp.Name = supInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = supInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = supInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = supInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else
                    {
                        supInfo = apl.Element(Getns2Name("nonResidentInfo"));
                        if (supInfo != null)
                        {
                            sp.Code = supInfo.Element(GetGlobalName("code"))?.Value;
                            sp.NRInfo = supInfo.Element(GetGlobalName("info"))?.Value;
                            sp.NonResident = "true";
                        }
                    }
                    sp.Offer = apl.Element(Getns2Name("price"))?.Value;
                    sp.Currency = apl.Element(Getns2Name("currency"))?.Element(GetGlobalName("code")).Value;

                    sp.Accepted = apl.Element(Getns2Name("accepted"))?.Value;
                    sp.PreventReason = apl.Element(Getns2Name("rejectionReason"))?.Value;
                    var wn = apl.Element(Getns2Name("winnerIndication"))?.Value;
                    if (wn == "W")
                        sp.Place = 1;
                    else
                        sp.Place = 0;

                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static List<LotInfo> ParsePurchaseProtocolVK(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();
            var lotsElemets = data.Elements(Getns2Name("protocolVKLotApplications"));

            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("lot"))?.Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("lot")).Element(Getns2Name("ordinalNumber")).Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();
                    var partInfo = apl.Element(Getns2Name("participantInfo"));
                    var supInfo = apl.Element(Getns2Name("supplierInfo"));
                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;
                    if (supInfo != null)
                    {
                        sp.Name = supInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = supInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = supInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = supInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else
                    {
                        supInfo = apl.Element(Getns2Name("nonResidentInfo"));
                        if (supInfo != null)
                        {
                            sp.Code = supInfo.Element(GetGlobalName("code"))?.Value;
                            sp.NRInfo = supInfo.Element(GetGlobalName("info"))?.Value;
                            sp.NonResident = "true";
                        }
                    }
                    sp.Offer = apl.Element(Getns2Name("price"))?.Value;
                    sp.Currency = apl.Element(Getns2Name("currency"))?.Element(GetGlobalName("code"))?.Value;
                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static List<LotInfo> ParsePurchaseProtocolPAEP(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();
            var lotsElemets = data.Elements(Getns2Name("protocolLotApplications"));

            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("ordinalNumber")).Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();
                    var supInfo = apl.Element(Getns2Name("supplierInfo"));

                    if (supInfo != null)
                    {
                        sp.Name = supInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = supInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = supInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = supInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else
                    {
                        supInfo = apl.Element(Getns2Name("nonResidentInfo"));
                        if (supInfo != null)
                        {
                            sp.Code = supInfo.Element(GetGlobalName("code"))?.Value;
                            sp.NRInfo = supInfo.Element(GetGlobalName("info"))?.Value;
                            sp.NonResident = "true";
                        }
                    }
                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;
                    sp.Offer = apl.Element(Getns2Name("lastPrice"))?.Value;
                    sp.Currency = apl.Element(Getns2Name("currency"))?.Element(GetGlobalName("code")).Value;
                    sp.Winner = true;
                    sp.Place = 1;
                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static List<LotInfo> ParsePurchaseProtocolPAAE(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();
            var lotsElemets = data.Elements(Getns2Name("protocolLotApplications"));

            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("lot")).Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("lot")).Element(Getns2Name("ordinalNumber")).Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();


                    var supInfo = apl.Element(Getns2Name("supplierInfo"));

                    if (supInfo != null)
                    {
                        sp.Name = supInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = supInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = supInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = supInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else
                    {
                        supInfo = apl.Element(Getns2Name("nonResidentInfo"));
                        if (supInfo != null)
                        {
                            sp.Code = supInfo.Element(GetGlobalName("code"))?.Value;
                            sp.NRInfo = supInfo.Element(GetGlobalName("info"))?.Value;
                            sp.NonResident = "true";
                        }
                    }
                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;
                    sp.Offer = apl.Element(Getns2Name("lastPrice"))?.Value;
                    sp.Currency = apl.Element(Getns2Name("currency"))?.Element(GetGlobalName("code")).Value;


                    var wn = apl.Element(Getns2Name("applicationPlace"))?.Value;
                    if (wn == "F")
                        sp.Place = 1;
                    else
                        sp.Place = 0;

                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static List<LotInfo> ParsePurchaseProtocolPAAE94(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();
            var lotsElemets = data.Elements(Getns2Name("protocolLotApplications"));

            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("lot")).Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("lot")).Element(Getns2Name("ordinalNumber")).Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();


                    var supInfo = apl.Element(Getns2Name("supplierInfo"));

                    if (supInfo != null)
                    {
                        sp.Name = supInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = supInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = supInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = supInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else
                    {
                        supInfo = apl.Element(Getns2Name("nonResidentInfo"));
                        if (supInfo != null)
                        {
                            sp.Code = supInfo.Element(GetGlobalName("code"))?.Value;
                            sp.NRInfo = supInfo.Element(GetGlobalName("info"))?.Value;
                            sp.NonResident = "true";
                        }
                    }
                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;
                    sp.Offer = apl.Element(Getns2Name("lastPrice"))?.Value;
                    sp.Currency = apl.Element(Getns2Name("currency"))?.Element(GetGlobalName("code")).Value;


                    var wn = apl.Element(Getns2Name("applicationPlace"))?.Value;
                    if (wn == "F")
                        sp.Place = 1;
                    else
                        sp.Place = 0;

                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static List<LotInfo> ParsePurchaseProtocolOSZ(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();
            var lotsElemets = data.Elements(Getns2Name("protocolLotApplications"));

            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("ordinalNumber")).Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();

                    var supInfo = apl.Element(Getns2Name("supplierInfo"));

                    if (supInfo != null)
                    {
                        sp.Name = supInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = supInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = supInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = supInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else
                    {
                        supInfo = apl.Element(Getns2Name("nonResidentInfo"));
                        if (supInfo != null)
                        {
                            sp.Code = supInfo.Element(GetGlobalName("code"))?.Value;
                            sp.NRInfo = supInfo.Element(GetGlobalName("info"))?.Value;
                            sp.NonResident = "true";
                        }
                    }
                    //sp.Offer = apl.Element(Getns2Name("lastPrice"))?.Value;
                    //sp.Currency = apl.Element(Getns2Name("currency"))?.Element(GetGlobalName("code")).Value;

                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;
                    var wn = apl.Element(Getns2Name("applicationPlace"))?.Value;
                    if (wn == "F")
                        sp.Place = 1;
                    else
                        sp.Place = 0;

                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static List<LotInfo> ParsePurchaseProtocolRZOK(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();
            var lotsElemets = data.Elements(Getns2Name("protocolLotApplications"));

            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("ordinalNumber")).Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();

                    var supInfo = apl.Element(Getns2Name("supplierInfo"));

                    if (supInfo != null)
                    {
                        sp.Name = supInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = supInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = supInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = supInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else
                    {
                        supInfo = apl.Element(Getns2Name("nonResidentInfo"));
                        if (supInfo != null)
                        {
                            sp.Code = supInfo.Element(GetGlobalName("code"))?.Value;
                            sp.NRInfo = supInfo.Element(GetGlobalName("info"))?.Value;
                            sp.NonResident = "true";
                        }
                    }
                    //sp.Offer = apl.Element(Getns2Name("lastPrice"))?.Value;
                    //sp.Currency = apl.Element(Getns2Name("currency"))?.Element(GetGlobalName("code")).Value;

                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;
                    sp.Accepted = apl.Element(Getns2Name("accepted"))?.Value;
                    sp.PreventReason = apl.Element(Getns2Name("rejectionReason"))?.Value;

                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static List<LotInfo> ParsePurchaseProtocolRZ1AE(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();
            var lotsElemets = data.Elements(Getns2Name("protocolLotApplications"));

            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("ordinalNumber")).Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();

                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;

                    sp.Accepted = apl.Element(Getns2Name("accepted"))?.Value;
                    sp.PreventReason = apl.Element(Getns2Name("rejectionReason"))?.Value;

                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static List<LotInfo> ParsePurchaseProtocolRZ2AE(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();
            var lotsElemets = data.Elements(Getns2Name("protocolLotApplications"));

            foreach (var el in lotsElemets)
            {
                LotInfo lot = new LotInfo();
                List<SupplierInfo> suppliers = new List<SupplierInfo>();

                lot.GUID = el.Element(Getns2Name("guid"))?.Value;
                lot.Number = Convert.ToInt32(el.Element(Getns2Name("ordinalNumber")).Value);
                var suppliersApls = el.Elements(Getns2Name("application"));
                foreach (var apl in suppliersApls)
                {
                    SupplierInfo sp = new SupplierInfo();

                    var supInfo = apl.Element(Getns2Name("supplierInfo"));

                    if (supInfo != null)
                    {
                        sp.Name = supInfo.Element(GetGlobalName("name"))?.Value;
                        sp.INN = supInfo.Element(GetGlobalName("inn"))?.Value;
                        sp.KPP = supInfo.Element(GetGlobalName("kpp"))?.Value;
                        sp.OGRN = supInfo.Element(GetGlobalName("ogrn"))?.Value;
                    }
                    else
                    {
                        supInfo = apl.Element(Getns2Name("nonResidentInfo"));
                        if (supInfo != null)
                        {
                            sp.Code = supInfo.Element(GetGlobalName("code"))?.Value;
                            sp.NRInfo = supInfo.Element(GetGlobalName("info"))?.Value;
                            sp.NonResident = "true";
                        }
                    }
                    sp.AplNum = apl.Element(Getns2Name("applicationNumber"))?.Value;
                    sp.Offer = apl.Element(Getns2Name("lastPrice"))?.Value;
                    sp.Currency = apl.Element(Getns2Name("currency"))?.Element(GetGlobalName("code")).Value;
                    sp.Accepted = apl.Element(Getns2Name("accepted"))?.Value;
                    sp.PreventReason = apl.Element(Getns2Name("rejectionReason"))?.Value;

                    suppliers.Add(sp);
                }
                lot.Suppliers = suppliers;
                lots.Add(lot);
            }
            return lots;
        }

        static void WriteData(ProtocolInfo prt, NpgsqlConnection conn)
        {
            string sql = $"insert into gz.protocol (guid, reg_num, version, protocol_type, date_birn, id_session, reg_num_notice, id_region) " +
                $"values ('{prt.GUID}', '{prt.RegNum}', {prt.Version}, '{prt.Type}', {RV.RD(prt.Date)}, {SnID}, '{prt.PurchaseRegNum}', {RegionID})" +
                $"on conflict (guid) do nothing;";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            int cnt = cmd.ExecuteNonQuery();
            cmd.Dispose();
            if (cnt != 0)
                foreach (var lot in prt.Lots)
                {
                    foreach (var sp in lot.Suppliers)
                    {
                        WriteSupplierData(sp, conn, out int supID);
                        sql = $"insert into gz.participation (guid_lot, id_supplier, suggestion, access, prevent_reason, id_session, place, guid_protocol, lot_num)" +
                            $"values ('{lot.GUID}', {supID}, '{RV.ChS(sp.Offer)}', '{sp.Accepted}', '{sp.PreventReason}', {SnID}, {sp.Place}, '{prt.GUID}', {lot.Number});";
                        cmd = new NpgsqlCommand(sql, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
        }

        static void WriteSupplierData(SupplierInfo sp, NpgsqlConnection conn, out int supplierID)
        {
            string select = $"select id from gz.supplier where inn = '{sp.INN}' and kpp = '{sp.KPP}' and code = '{sp.Code}' and non_resident is {RV.RB(sp.NonResident)} and nr_info = '{RV.ChQ(sp.NRInfo)}';";
            NpgsqlCommand cmd = new NpgsqlCommand(select, conn);
            object id = cmd.ExecuteScalar();
            if (id == null)
            {
                string insert = $"insert into gz.supplier (naimen, inn, kpp, ogrn, id_session, non_resident, code, nr_info)" +
                     $"values  ({RV.RSt(sp.Name)}, '{sp.INN}', '{sp.KPP}', '{sp.OGRN}', {SnID}, {RV.RB(sp.NonResident)}, '{sp.Code}', '{RV.ChQ(sp.NRInfo)}')" +
                     $"on conflict (inn, kpp, non_resident, code, nr_info) do nothing;";
                NpgsqlCommand cmdInsert = new NpgsqlCommand(insert, conn);
                int count = cmdInsert.ExecuteNonQuery();
                supplierID = (int)cmd.ExecuteScalar();
                cmd.Dispose();
            }
            else
            {
                string update = $"update gz.supplier set ogrn = {RV.RSt(sp.OGRN)} where id = {id};";
                cmd = new NpgsqlCommand(update, conn);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                supplierID = (int)id;
            }
        }








    }
}
