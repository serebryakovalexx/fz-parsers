﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using static xmlParser223.DocumentsInfo.ContractCompletingInfo;
using static xmlParser223.RV;
using System.Xml.Linq;
using System.IO;
using static xmlParser223.ConnectionController;
using xmlParser223.DocumentsInfo;
using System.IO.Compression;
using System.Text.RegularExpressions;

namespace xmlParser223.ParserXML
{

    public class ContractCompletingLoader
    {
        static int SnID { get; set; }
        static int RegionID { get; set; }
        static XName GetGlobalName(string prop) => XName.Get(prop, "http://zakupki.gov.ru/223fz/types/1");
        public static XName Getns2Name(string prop) => XName.Get(prop, "http://zakupki.gov.ru/223fz/contract/1");

        public static void ParseCompletings(string regionPath, int snId, int regionId)
        {
            SnID = snId;
            RegionID = regionId;

            string directoryPath = Directory.GetDirectories(regionPath).Where(s => s.Equals("contractCompleting")).FirstOrDefault();
            List<string> filePaths = Directory.GetFiles(directoryPath).ToList();
            List<Task> taskList = new List<Task>();
            foreach (var filePath in filePaths)
                taskList.Add(Task.Factory.StartNew(() => ParseCompleting(filePath)));
            Task.WaitAll(taskList.ToArray());


        }


        public static void ParseZipCompletings(string regionPath, int snId, int regionId)
        {
            SnID = snId;
            RegionID = regionId;

            string directoryPath = Directory.GetDirectories(regionPath).Where(s => s.Contains("contractCompleting")).FirstOrDefault();

            List<string> zipFiles = Directory.GetFiles(directoryPath + @"\daily").ToList();
            List<Task> taskList0 = new List<Task>();
            foreach (var zipFile in zipFiles)
                taskList0.Add(Task.Factory.StartNew(() =>
                {
                    try
                    {
                        using (ZipArchive archive = ZipFile.OpenRead(zipFile))
                        {
                            Regex rg = new Regex(@"daily_\d+");
                            List<Task> taskList = new List<Task>();
                            foreach (ZipArchiveEntry file in archive.Entries)
                                using (StreamReader reader = new StreamReader(file.Open()))
                                {
                                    string data = reader.ReadToEnd();
                                    taskList.Add(Task.Factory.StartNew(() =>
                                    {
                                        string fileName = zipFile + rg.Match(file.Name).Value.Replace("daily", "");
                                        ParseCompleting(data, fileName);
                                    }));
                                }
                            Task.WaitAll(taskList.ToArray());
                        }
                    }
                    catch (Exception err)
                    {
                        File.AppendAllText("invalidFiles.txt", zipFile + "\t" + err.Message + Environment.NewLine);
                    }
                }));
            Task.WaitAll(taskList0.ToArray());


        }

        public static void ParseCompleting(string data, string fileName)
        {

            if (data != "")
            {
                string sql;
                NpgsqlCommand cmd;

                XDocument doc = XDocument.Parse(data);
                string docType = doc.Root.Name.LocalName;
                XElement docTypeData = doc.Root
                                 .Element(Getns2Name("body"))
                                 .Element(Getns2Name("item"))
                                 .Element(Getns2Name($"{docType}Data"));

                if (docTypeData != null)
                {
                    OpenFreeConn(out var a, out var cn);
                    try
                    {
                        ContractCompletingInfo cmpl = ContractCompleting(docTypeData);
                        WriteCompletingData(cmpl, cn);
                        sql = $"insert into gz.file_status (id, id_session, load_status, file_code, guid_document) values ('{fileName}', {SnID}, 1, 4, '{cmpl.GUID}') ";
                        cmd = new NpgsqlCommand(sql, cn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    catch (Exception ex)
                    {
                        sql = $"insert into gz.file_status (id, id_session, load_status, error ,file_code) values ('{fileName}','{SnID}', 0, '{ex.Message}', 2) on conflict(id) do nothing";
                        cmd = new NpgsqlCommand(sql, cn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    CloseBusyConn(a, cn);
                }
            }


        }



        public static void ParseCompleting(string filePath)
        {
            string file = File.ReadAllText(filePath);
            if (file != "")
            {
                XDocument doc = XDocument.Parse(file);
                string docType = doc.Root.Name.LocalName;
                XElement docTypeData = doc.Root
                                 .Element(Getns2Name("body"))
                                 .Element(Getns2Name("item"))
                                 .Element(Getns2Name($"{docType}Data"));
                if (docTypeData != null)
                {
                    OpenFreeConn(out var a, out var cn);
                    WriteCompletingData(ContractCompleting(docTypeData), cn);
                    CloseBusyConn(a, cn);
                }
            }
        }

        public static ContractCompletingInfo ContractCompleting(XElement data)
        {
            ContractCompletingInfo cmpl = new ContractCompletingInfo();
            cmpl.Type = data.Name.LocalName;
            cmpl.GUID = data.Element(Getns2Name("guid")).Value;
            cmpl.RegNum = data.Element(Getns2Name("registrationNumber")).Value;
            cmpl.Date = DateTime.Parse(data?.Element(Getns2Name("createDateTime"))?.Value ?? "01.01.1900");
            cmpl.PubDate = DateTime.Parse(data?.Element(Getns2Name("publicationDate"))?.Value ?? "01.01.1900");
            cmpl.Modification = data.Element(Getns2Name("modificationDescription"))?.Value;
            cmpl.Version = Convert.ToInt32(data?.Element(Getns2Name("version"))?.Value ?? "-1");
            cmpl.Completed = data.Element(Getns2Name("completed"))?.Value;
            cmpl.HasPenalty = data.Element(Getns2Name("hasPenalty"))?.Value;
            cmpl.PenaltyInfo = data?.Element(Getns2Name("PenaltyInfo"))?.Value;
            cmpl.ContractRegNum = data.Element(Getns2Name("contractRegNumber"))?.Value;
            cmpl.ContractGUID = data?.Element(Getns2Name("contractInfo"))?.Element(Getns2Name("guid"))?.Value;


            if (cmpl.Type == "contractCancelation")
            {
                cmpl.CancelationCode = Convert.ToInt32(data?.Element(Getns2Name("baseContractCancellationCode"))?.Value ?? "-1");
                cmpl.CancelationName = data.Element(Getns2Name("baseContractCancellationName")).Value;
                cmpl.RejectionDocCode = Convert.ToInt32(data?.Element(Getns2Name("baseRejectionDocCode"))?.Value ?? "-1");
                cmpl.RejectionDocName = data.Element(Getns2Name("baseRejectionDocName")).Value;
                cmpl.CompletionDate = DateTime.Parse(data?.Element(Getns2Name("completionDate"))?.Value ?? "01.01.1900");
                cmpl.RejectionDocNum = data.Element(Getns2Name("baseRejectionDocNum")).Value;
                cmpl.RejectionDocDate = DateTime.Parse(data?.Element(Getns2Name("baseRejectionDocDate"))?.Value ?? "01.01.1900");
            }
            return cmpl;
        }

        public static void WriteCompletingData(ContractCompletingInfo cmpl, NpgsqlConnection cn)
        {
            string sql = $"insert into gz.completing (type, guid, reg_num, version, modification, create_date, " +
                $"publish_date, completed, has_penalty, penalty_info, guid_contract, reg_num_contract, cancelation_code, " +
                $"cancelation_name, rejection_doc_code, rejection_doc_name, completion_date, rejection_doc_num, " +
                $"rejection_doc_date, id_session, id_region) " +
                $"values ('{cmpl.Type}', '{cmpl.GUID}', '{cmpl.RegNum}', {RInt(cmpl.Version)}, {RSt(cmpl.Modification)}, " +
                $"{RD(cmpl.Date)}, {RD(cmpl.PubDate)}, {RSt(cmpl.Completed)}, {cmpl.HasPenalty}, {RSt(cmpl.PenaltyInfo)}, " +
                $"{RSt(cmpl.ContractGUID)}, {RSt(cmpl.ContractRegNum)}, " +
                $"{RInt(cmpl.CancelationCode)}, {RSt(cmpl.CancelationName)}, {RInt(cmpl.RejectionDocCode)}, " +
                $"{RSt(cmpl.RejectionDocName)}, {RD(cmpl.CompletionDate)}, {RSt(cmpl.RejectionDocNum)}, " +
                $"{RD(cmpl.RejectionDocDate)}, {SnID}, {RegionID}) " +
                $"on conflict (guid) do nothing returning id;";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, cn);
            object id = cmd.ExecuteScalar();
            if (id != null)
            {
                sql = $"select guid from gz.contract where guid = '{cmpl.ContractGUID}'";
                cmd = new NpgsqlCommand(sql, cn);
                object check = cmd.ExecuteScalar();
                if (check != null)
                {
                    sql = $"update gz.contract set id_completing = {(int)id} where guid = '{cmpl.ContractGUID}'";
                    cmd = new NpgsqlCommand(sql, cn);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
