﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlParser223.DocumentsInfo
{
    public class ContractCompletingInfo
    {

        public string GUID { get; set; }
        public string RegNum { get; set; }
        public int Version { get; set; }
        public string Modification { get; set; }
        public string Type { get; set; }
        public string ContractGUID { get; set; }
        public string ContractRegNum { get; set; }
        public DateTime Date { get; set; }
        public DateTime PubDate { get; set; }
        public DateTime CompletionDate { get; set; }
        
        public DateTime RejectionDocDate { get; set; }
        public string Completed { get; set; }
        public string HasPenalty { get; set; }
        public string PenaltyInfo { get; set; }
        public int CancelationCode { get; set; }
        public string CancelationName { get; set; }
        public int RejectionDocCode { get; set; }
        public string RejectionDocName { get; set; }
        public string RejectionDocNum { get; set; }
    }
}
